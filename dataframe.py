import pandas as pd

CLASS_KEYWORD = "class"

class DataFrame:
    def __init__(self, filename, normalize=False):
        df = pd.read_csv(filename)
        class_column = df[CLASS_KEYWORD]
        self.classes = class_column.unique()
        self.classes.sort()
        self.output = self.to_vectors(class_column)
        input_df = df.drop(CLASS_KEYWORD, axis=1)
        if normalize:
            input_df = (input_df - input_df.min()) / (input_df.max() - input_df.min())
        self.input = input_df

    def to_vectors(self, class_column):
        vectors = map(
            lambda x: [1 if x == class_label else 0 for class_label in self.classes],
            class_column
        )
        return pd.DataFrame(data=list(vectors), columns=self.classes)

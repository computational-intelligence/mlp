#!/usr/bin/python3

from classes.network import Network
from dataframe import DataFrame
from configparser import ConfigParser

if __name__ == '__main__':
    config = ConfigParser()
    config.read("params.ini")

    training_data = DataFrame("data/iris/IrisDataTrain.csv", normalize=True)
    testing_data = DataFrame("data/iris/IrisDataTest.csv", normalize=True)
    network = Network([4, 5, 3], config["iris"])
    network.train(training_data)
    network.test(testing_data)

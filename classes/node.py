class Node:
    def __init__(self):
        self.inputs = []
        self.outputs = []
        self.value = None

    def connect_input(self, input):
        self.inputs.append(input)

    def connect_output(self, output):
        self.outputs.append(output)
    
    def spike(self):
        for synapse in self.outputs:
            synapse.update_value(self.value)
    
    def update_delta(self, delta):
        pass

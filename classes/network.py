from .layer import Layer
from .input_layer import InputLayer
from .output_layer import OutputLayer
from dataframe import DataFrame
from statistics import mean

class Network:
    def __init__(self, topology, config):
        self.config = config
        init_range = float(self.config["INIT_RANGE"])
        self.input_layer = InputLayer(init_range, topology[0])
        self.biases = []
        self.hidden_layers = []
        for n in topology[1:-1]:
            new_layer = Layer(init_range, n)
            self.connect_layer(new_layer)
            self.hidden_layers.append(new_layer)
        self.output_layer = OutputLayer(init_range, topology[-1])
        self.connect_layer(self.output_layer)
        
    def connect_layer(self, new_layer):
        previous_layer = self.hidden_layers[-1] if self.hidden_layers else self.input_layer
        previous_layer.connect_to_layer(new_layer)
        if self.config.getboolean("BIAS"):
            biases = [neuron.add_bias(float(self.config["INIT_RANGE"])) for neuron in new_layer.nodes]
            self.biases.extend(biases)

    def train(self, training_data):
        self.set_data(training_data)
        iterations = int(self.config.get("MAX_ITER", len(self.input_matrix)))
        learn_rate = float(self.config["LEARN_RATE"])
        for _ in range(int(self.config["EPOCHS"])):
            for i in range(iterations):
                self.run(self.input_matrix[i], self.desired_output_matrix[i])
                for layer in [self.output_layer] + self.hidden_layers[::-1]:
                    layer.backpropagate(learn_rate)

    def set_data(self, data: DataFrame):
        self.input_matrix = data.input.as_matrix()
        self.output_layer.set_labels(data.classes)
        self.desired_output_matrix = data.output.as_matrix()

    def run(self, input_vector, desired_output_vector=None):
        self.input_layer.set_input_vector(input_vector)
        if desired_output_vector is not None:
            self.output_layer.set_desired_output(desired_output_vector)
        network = self.biases + [self.input_layer] + self.hidden_layers + [self.output_layer]
        for element in network:
            element.spike()
        return self.output_layer.get_output()

    def test(self, testing_data):
        self.set_data(testing_data)
        iterations = len(self.input_matrix)
        correct_output_counter = 0
        error_sum = 0
        for i in range(iterations):
            input = self.input_matrix[i]
            correct_output_vector = self.desired_output_matrix[i]
            correct_output = self.output_layer.label(correct_output_vector)
            network_output = self.run(input, correct_output_vector)
            error_sum += self.output_layer.error
            if network_output == correct_output:
                correct_output_counter += 1
            if not self.config.getboolean("SILENT_TESTING"):
                print(self.output_layer.format())

        print("Data length: {}".format(iterations))
        print("Network accuracy: {:.2%}".format(1 - (error_sum / iterations)))
        print("Correct guesses: {:.2%}".format(correct_output_counter / iterations))


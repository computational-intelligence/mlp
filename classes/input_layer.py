from .layer import Layer
from .input import Input

class InputLayer(Layer):
    def __init__(self, init_range, number_of_neurons=None, input_vector=None):
        self.init_range = init_range
        if input_vector:
            self.nodes = [Input(x) for x in input_vector]
        else:
            self.nodes = []
            for _ in range(number_of_neurons):
                self.add(Input())

    def set_input_vector(self, input_vector):
        for i, v in enumerate(input_vector):
            self.nodes[i].set_value(v)

    def backpropagate(self, learn_rate):
        pass

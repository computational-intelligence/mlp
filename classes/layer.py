from .neuron import Neuron
from .synapse import Synapse

class Layer:
    def __init__(self, init_range, number_of_neurons):
        self.init_range = init_range
        self.nodes = []
        for _ in range(number_of_neurons):
            self.add(Neuron())

    def add(self, node):
        self.nodes.append(node)
    
    def connect_to_neuron(self, neuron):
        for node in self.nodes:
            Synapse(self.init_range, input=node, output=neuron)
    
    def connect_to_layer(self, layer):
        for neuron in layer.nodes:
            self.connect_to_neuron(neuron)

    def spike(self):
        for node in self.nodes:
            node.spike()

    def backpropagate(self, learn_rate):
        for node in self.nodes:
            node.backpropagate(learn_rate)

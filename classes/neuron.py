import math
from enum import Enum
from .node import Node
from .input import Input
from .synapse import Synapse

class Functions(Enum):
    SIGMOID = lambda x: 1 / (1 + math.exp(-x))

class Neuron(Node):
    def __init__(self, activation_function=Functions.SIGMOID):
        super(Neuron, self).__init__()
        self.delta = 0
        self.activation_function = activation_function

    def spike(self):
        self.sum = sum(s.value for s in self.inputs)
        self.value = self.activation_function(self.sum)
        super(Neuron, self).spike()

    def backpropagate(self, learn_rate):
        output_coefficient = (1 - self.value) * self.value
        for synapse in self.inputs:
            synapse.backpropagate(self.delta, learn_rate, output_coefficient)
        self.delta = 0
    
    def update_delta(self, delta):
        self.delta += delta

    def add_bias(self, init_range):
        bias = Input(1)
        Synapse(init_range, bias, self)
        return bias

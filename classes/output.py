class Output:  
    def __init__(self, label=None):
        self.set_label(label)
        self.desired_value = None
        self.input = None

    def set_label(self, label):
        self.label = label

    def set_desired_value(self, desired_value):
        self.desired_value = desired_value

    def connect_input(self, input):
        self.input = input

    def update_value(self):
        self.value = self.input.value
        if self.desired_value != None:
            self.delta = self.desired_value - self.value
            self.input.backpropagate(self.delta)

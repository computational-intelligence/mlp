from .node import Node

class Input(Node):
    def __init__(self, value=None):
        super(Input, self).__init__()
        self.set_value(value)

    def set_value(self, value):
        self.value = value

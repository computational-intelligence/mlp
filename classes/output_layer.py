from .layer import Layer
from .output import Output
from .synapse import Synapse
from statistics import mean

class OutputLayer(Layer):
    def __init__(self, init_range, number_of_neurons):
        super(OutputLayer, self).__init__(init_range, number_of_neurons)
        self.outputs = [Output() for _ in self.nodes]
        for i, neuron in enumerate(self.nodes):
            Synapse(init_range, neuron, self.outputs[i])
    
    def set_labels(self, class_labels):
        self.labels = class_labels
        for i, output in enumerate(self.outputs):
            output.set_label(class_labels[i])

    def set_desired_output(self, desired_output_vector):
        for i, output in enumerate(self.outputs):
            output.set_desired_value(desired_output_vector[i])
        self.desired_output_vector = desired_output_vector
        self.desired_output = self.label(desired_output_vector)

    def get_output(self):
        return self.label(self.output_vector)
    
    def spike(self):
        super(OutputLayer, self).spike()
        for output in self.outputs:
            output.update_value()
        self.output_vector = [output.value for output in self.outputs]
        self.output = self.label(self.output_vector)
        self.error = mean([abs(output.delta) for output in self.outputs])

    def format(self):
        output_vector_string = "[" + ", ".join(["{:.2f}".format(x) for x in self.output_vector]) + "]"
        return ("Correct value: {}\n"
                "Selected value: {}\n"
                "Network output: {}\n"
               ).format(self.desired_output, self.output, output_vector_string)
    
    def label(self, vector):
        return self.labels[list(vector).index(max(vector))]

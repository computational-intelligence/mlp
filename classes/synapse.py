import random
import sys, os
from .output import Output

class Synapse:
    def __init__(self, init_range=0, input=None, output=None):
        self.weight = random.uniform(-init_range, init_range)
        if input:
            self.input = input
            self.input.connect_output(self)
        if output:
            self.output = output
            self.output.connect_input(self)
            if type(output) is Output:
                self.weight = 1

    def update_value(self, value):
        self.value = value * self.weight
    
    def backpropagate(self, delta, learn_rate=0, output_coefficient=1):
        if learn_rate:
            update = -learn_rate * delta * output_coefficient * self.input.value
            self.weight -= update
        self.input.update_delta(delta * self.weight * output_coefficient)

import unittest
from dataframe import DataFrame

filename = 'data/iris/IrisDataAll.csv'

class TestDataFrame(unittest.TestCase):
    def setUp(self):
        self.headers = ['leaf-length', 'leaf-width', 'petal-length', 'petal-width']
        self.classes = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']
        self.data_input = [5.1, 3.5, 1.4, 0.2]
        self.data_output = [1, 0, 0]
        self.dataframe = DataFrame(filename)

    def test_init(self):
        self.assertEqual(self.headers, self.dataframe.input.columns.tolist())
        self.assertEqual(self.classes, list(self.dataframe.classes))
        self.assertEqual(self.data_input, list(self.dataframe.input.iloc[0]))
        self.assertEqual(self.data_output, list(self.dataframe.output.iloc[0]))
        
    def test_normalize(self):
        normalized_dataframe = DataFrame(filename, normalize=True)
        normality = [x>=0 and x<=1 for x in list(normalized_dataframe.input.iloc[0])]
        self.assertTrue(all(normality))

import unittest
from classes.synapse import Synapse
from classes.neuron import Neuron
from classes.output import Output

class TestSynapse(unittest.TestCase):        
    def test_init(self):
        ABS_WEIGHT = 0.1
        input_neuron = Neuron()
        output_neuron = Neuron()
        synapse = Synapse(ABS_WEIGHT, input_neuron, output_neuron)

        self.assertGreater(synapse.weight, -ABS_WEIGHT)
        self.assertLess(synapse.weight, ABS_WEIGHT)
        self.assertIn(synapse, input_neuron.outputs)
        self.assertIn(synapse, output_neuron.inputs)

    def test_propagate_only_delta(self):
        input_neuron = Neuron()
        output = Output("label")
        synapse = Synapse(input=input_neuron, output=output)
        synapse.update_value(0)
        output.set_desired_value(1)
        output.update_value()
        self.assertEqual(input_neuron.delta, 1)
